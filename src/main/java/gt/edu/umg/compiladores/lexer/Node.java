/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.edu.umg.compiladores.lexer;

/**
 *
 * @author luisc
 */
public class Node {
    
    private Node left;
    private Node right;
    private String value;
    private String pre;
    private int id;

    public Node(String v, String p, int i) {
        
        this.left = null; 
        this.value = null;
        this.value = v;
        this.pre = p;
        this.id = i;
        
    }

    public Node(Node left, Node right, String value, String p, int i) {
        this.left = left;
        this.right = right;
        this.value = value;
        this.pre = p;
        this.id = i;
    }

    public int getId() {
        return id;
    }

    
    public String getPre() {
        return pre;
    }

    public void setPre(String pre) {
        this.pre = pre;
    }

    
    
    public String getValue() {
        return value;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }
    
    
    
    
}
