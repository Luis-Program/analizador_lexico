import java_cup.runtime.Symbol;
import java.io.*;
import java.lang.*;
%%

integer=[0-9]+
erase="-"
plus="+"
per="*"
split="/"
bracket_open="("
bracket_close=")"
semicolon = ";"

%cup
%line
%char
%class Scanner
%ignorecase
%%

{integer} {return new Symbol(sym.integer,new Token(sym.integer, new String(yytext()), yyline, yychar));}
{erase} {return new Symbol(sym.erase,new Token(sym.erase, new String(yytext()), yyline, yychar));}
{per} {return new Symbol(sym.per,new Token(sym.per, new String(yytext()), yyline, yychar));}
{plus} {return new Symbol(sym.plus,new Token(sym.plus, new String(yytext()), yyline, yychar));}
{split} {return new Symbol(sym.split,new Token(sym.split, new String(yytext()), yyline, yychar));}
{bracket_open} {return new Symbol(sym.bracket_open,new Token(sym.bracket_open, new String(yytext()), yyline, yychar));}
{bracket_close} {return new Symbol(sym.bracket_close,new Token(sym.bracket_close, new String(yytext()), yyline, yychar));}
{semicolon} {return new Symbol(sym.semicolon,new Token(sym.semicolon, new String(yytext()), yyline, yychar));}

[\b\t\r\f\n]+ {}
" "+ {}
. {System.out.println("Error Lexico Identificado "+ yytext() + " en la linea "+ yyline +" y posicion " + yychar);}

