package Grap_Pos_Pre;


//----------------------------------------------------
// The following code was generated by CUP v0.10k
// Wed May 12 08:50:45 CST 2021
//----------------------------------------------------

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int erase = 3;
  public static final int per = 5;
  public static final int semicolon = 9;
  public static final int EOF = 0;
  public static final int integer = 2;
  public static final int plus = 4;
  public static final int split = 6;
  public static final int error = 1;
  public static final int bracket_close = 8;
  public static final int bracket_open = 7;
}

